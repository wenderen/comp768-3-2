var ringRadius;

var axes, simulation, bead, ring;

var scene, camera, fieldOfView, aspectRatio, nearPlane, farPlane, HEIGHT, WIDTH, renderer, container;

var animationStarted = false;

function degToRad(deg) {
    return deg * 2 * Math.PI / 360;
}

class Bead {
    constructor() {
        this.RADIUS = 0.1;

        this.dt = simulation.timeStep;

        var material = new THREE.MeshLambertMaterial({ color: 0xff0000 });
        var geometry = new THREE.SphereGeometry(this.RADIUS,32,32);
        this.mesh =  new THREE.Mesh(geometry, material);

        // Initialize mesh position in space.
        this.mesh.position.x = Math.sin(this.theta);
        this.mesh.position.y = 0; // This never changes.
        this.mesh.position.z = Math.cos(this.theta);

        // These 2 quantities uniquely describe the system.
        this.theta = degToRad(simulation.initialTheta); // Clockwise angle made with positive y-axis.
        this.v = simulation.initialSpeed; // Magnitude of velocity of the bead.
    }

    update() {
        // Update state quantities
        this.v += 9.8 * Math.sin(bead.theta) * this.dt;
        this.theta += (this.v / ringRadius) * this.dt;

        // Update rendered position
        this.mesh.position.x = Math.sin(this.theta);
        this.mesh.position.z = Math.cos(this.theta);
    }
}

class Ring {
    constructor() {
        this.RADIUS = 1;
        this.THICKNESS = 0.03;

        var material = new THREE.MeshBasicMaterial({ color: 0x000000 });
        var geometry = new THREE.RingGeometry(this.RADIUS-this.THICKNESS/2, this.RADIUS+this.THICKNESS/2, 64);
        this.mesh = new THREE.Mesh(geometry, material);

        this.mesh.position.x = 0;
        this.mesh.position.y = 0;
        this.mesh.position.z = 0;

        this.mesh.rotation.x = Math.PI / 2;
    }
}

function resetEverything() {
    animationStarted = false;
    scene.remove(bead.mesh);
    createBead(simulation);
}

class RingSimulation {
    constructor() {
        // this.showAxes = true;
        // this.showRing = true;
        this.paused = false;
        this.initialSpeed = 0.1;
        this.initialTheta = 0;
        this.timeStep = 1e-2;
        this.play = function () {
            createBead();
            renderer.render(scene, camera);
            animationStarted = true;
            loop();
        }
    }
}

function createScene() {
    HEIGHT = window.innerHeight;
    WIDTH = window.innerWidth;
    scene = new THREE.Scene();
    scene.fog = new THREE.Fog(0xf7d9aa, 100, 950);
    aspectRatio = WIDTH / HEIGHT;
    fieldOfView = 60;
    nearPlane = 1;
    farPlane = 10000;
    camera = new THREE.PerspectiveCamera(fieldOfView, aspectRatio, nearPlane, farPlane);

    // face the cannon head-on when it is at 0 elevation
    camera.position.x = -1;
    camera.position.y = -2;
    camera.position.z = 2;

    camera.up = new THREE.Vector3(0,0,1);
    camera.lookAt(new THREE.Vector3(0,0,0));
    renderer = new THREE.WebGLRenderer({
        alpha: true,
        antialias: true
    });
    renderer.setSize(WIDTH, HEIGHT);
    renderer.shadowMap.enabled = true;
    container = document.getElementById('scene');
    container.appendChild(renderer.domElement);
    window.addEventListener('resize', handleWindowResize, false);
}

function createLights() {
    var ambientLight = new THREE.AmbientLight(0x404040);
    var hemisphereLight = new THREE.HemisphereLight(0xaaaaaa, 0x000000, .9);
    var directionalLight = new THREE.DirectionalLight(0xffffff, 0.5);
    var pointLight = new THREE.PointLight(0xffffff, 1, 100);
    pointLight.position.set(0,0,5);
    directionalLight.position.set(0,0,5);
    scene.add(ambientLight);
    scene.add(hemisphereLight);
    scene.add(directionalLight);
    scene.add(pointLight);
}

function handleWindowResize() {
    HEIGHT = window.innerHeight;
    WIDTH = window.innerWidth;
    renderer.setSize(WIDTH, HEIGHT);
    camera.aspect = WIDTH / HEIGHT;
    camera.updateProjectionMatrix();
}

class Axes {
    constructor() {
        this.mesh = new THREE.Object3D();
        const axisHalfLength = 400;
        const axisRadius = 0.25;

        var geometry = new THREE.Geometry();
        geometry.vertices.push(new THREE.Vector3(-axisHalfLength,0,0));
        geometry.vertices.push(new THREE.Vector3(axisHalfLength,0,0));

        const xMaterial = new THREE.MeshBasicMaterial({ color: 0xff0000 });
        const yMaterial = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
        const zMaterial = new THREE.MeshBasicMaterial({ color: 0x0000ff });

        var xLine = new THREE.Line(geometry, xMaterial);
        var yLine = new THREE.Line(geometry, yMaterial);
        var zLine = new THREE.Line(geometry, zMaterial);
        yLine.rotation.z = Math.PI/2;
        zLine.rotation.y = -Math.PI/2;

        this.mesh.add(xLine);
        this.mesh.add(yLine);
        this.mesh.add(zLine);
    }
}

function createBead() {
    bead = new Bead();
    scene.add(bead.mesh);
    renderer.render(scene, camera);
}

function createRing() {
    ring = new Ring();
    scene.add(ring.mesh);
}

function createAxes() {
    axes = new Axes();
    scene.add(axes.mesh);
}

window.addEventListener('load', init, false);

function loop() {
    // axes.mesh.visible = simulation.showAxes;
    // ring.mesh.visible = simulation.showRing;
    if (bead != undefined)
        bead.dt = simulation.timeStep;
    if (!simulation.paused && animationStarted)
        bead.update();
    renderer.render(scene, camera);
    requestAnimationFrame(loop);
}

function initGUI() {
    simulation = new RingSimulation();
    var gui = new dat.GUI();
    gui.add(simulation, 'initialTheta', 0, 359);
    gui.add(simulation, 'initialSpeed');
    gui.add(simulation, 'timeStep');
    gui.add(simulation, 'play');
}

function init() {
    ringRadius = 1;
    initGUI();
    createScene();
    createAxes();
    createLights();
    createRing();
    loop();
}